package Astar;

//A*アルゴリズム
public class Astar {
	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		/*
		 * 初期状態
		 * S→0
		 * G→4
		 *
		 * コスト
		 * 山　　→3
		 * 森　　→2
		 * 空欄　→1
		 */
		//初期状態
		int start[][]={{1,1,1,1,1},
			       	   {1,1,3,1,1},
			       	   {1,1,3,2,1},
			       	   {0,2,3,2,1},
			       	   {1,1,3,2,4},
			       	   {1,1,3,2,1},
			       	   {1,1,2,1,1},
			       	   {1,1,1,1,1}};

		//現在までのコスト
		int move[][]={{17,16,15,14,13},
			          {18,17,14,13,12},
			          {19,18,15,13,11},
			          {20,18,15,13,10},
			          {19,18,15,13,9},
			          {18,17,14,12,10},
			          {17,16,14,12,11},
			          {16,15,14,13,12}};
		
		//未来のコスト
		int future[][]={{12,13,14,15,16},
						{13,14,15,16,17},
						{14,15,16,17,18},
						{15,16,17,18,19},
						{16,17,18,19,20},
						{15,16,17,18,19},
						{14,15,16,17,18},
						{13,14,15,16,17},};
		
		//合計値代入
		int sum[][] = new int[8][5];

		int i,j;

		//初期状態表示
		System.out.println("初期状態表示");
		for( i=0;i<5;i++){
			for( j=0;j<8;j++){
				switch(start[j][i]){
				case 0:System.out.print("S");break;
				case 1:System.out.print("⬜️️");break;
				case 2:System.out.print("森");break;
				case 3:System.out.print("山");break;
				case 4:System.out.print("G");break;
				}
			}
			System.out.print("\n");
		}
		System.out.print("\n");
		
		//現在のコスト表示
		System.out.println("現在のコスト");
		for(i=0;i<5;i++){
			for(j=0;j<8;j++){
				System.out.print(move[j][i]+" ");
			}
			System.out.print("\n");
		}
		System.out.print("\n");
		
		//未来のコスト表示
		System.out.println("未来のコスト");
		for(i=0;i<5;i++){
			for(j=0;j<8;j++){
				System.out.print(future[j][i]+" ");
			}
			System.out.print("\n");
		}
		System.out.print("\n");
		
		//合計値計算(コスト算出)
		System.out.println("合計値");
		for(i=0;i<5;i++){
			for(j=0;j<8;j++){
				sum[j][i]=move[j][i]+future[j][i];
				System.out.print(sum[j][i]+" ");
			}
			System.out.print("\n");
		}
		System.out.print("\n");
		
		
		//初期状態
		//配列の方法で計算
		int sx=3;//スタート地点のX座標
		int sy=0;//スタート地点のY座標
		int gx=4;//ゴール地点のX座標
		int gy=4;//ゴール地点のY座標
		
		//探索用変数
		int xs=sx,xnow=0,x=0;//xs:スタート地点のX座標 Xnow:今のX座標 x:初期値
		int ys=sy,ynow=0,y=0;//ys:スタート地点のY座標 Ynow:今のY座標 y:初期値
		int cost=sum[xs][ys];//最初の座標のコストを代入
		int next=cost;//コストを代入
		System.out.print("("+(xs)+","+(ys)+")\n");//最初の座標表示
		
		//スタートはsum[0][3]
		//最短経路の座標表示プログラム
		while(true){
			//今の座標(x,y)の上下左右
			for(i=0;i<4;i++){
				switch(i){
				case 0://上
					xnow=xs-1;
					ynow=ys-1;
					break;
				case 1://下
					xnow=xs;
					ynow=ys+1;
					break;
				case 2://左
					xnow=xs-1;
					ynow=ys;
					break;
				case 3://右
					xnow=xs+1;
					ynow=ys;
					break;
				}
				if(xnow>0 && x<8 && ynow>0 && ynow<5 ){//マップ中
					next = sum[xnow][ynow];	//0~3までのコストを比較
					if(next<=cost){//最初に代入されたコストと比較
						cost=next;
						x=xnow;
						y=ynow;
					}
				}
			}
			xs=x;//次の座標値とする
			ys=y;//次の座標値とする
			
			//結果表示(配列表示)
			if((x==gx)&&(y==gy)){//ゴールの座標になった時
				System.out.println("("+(x)+","+(y)+")");//座標表示
				break;
			}
			else{//それ以外
				System.out.println("("+(xs)+","+(ys)+")");//座標表示			
			}
		}
	}
}