package kadai3;

import java.util.Scanner;

public class kadai3 {
	
	public static void main(String[] args) {
		new kadai3();
	}

	kadai3(){
		zinkoutinou zinkoutinou = new zinkoutinou();
		while(true){
			zinkoutinou.init();
			zinkoutinou.test();
		}
	}
}

//人工知能クラス
class zinkoutinou {
	
	//クラス変数
	human A;
	shibainu Taro;
	human human=new human();
	dog dog=new dog();

	
	//type識別変数
	String type;
	
	//質問タイプ識別
	//String ha="は";
	String no="の";
	String san="さん";
	String dearu="である";
	
	//単語を小分けにする配列
	String tango[]=new String[8];
	
	//探索用変数群
	String word[] = {"Aさん","ペット","タロー","性格"};
	String str1="なんですか";
	String str2="は";
	String str3="の";
	String Asan="Aさん";
	String pet="ペット";
	String taro="タロー";
	String seikaku="性格";
	
	public void init(){
		for(int i=0;i<tango.length;i++){
			tango[i]=null;
		}
	}
	
	public void test() {
		//"Ａさん"の情報を持つオブジェクト
		A = new human("Ａさん", "タロー");
		//"タロー"の情報を持つオブジェクト
		Taro = new shibainu("タロー");
		//結果取得する変数
		String ans = "分からない";
		
		//入力変数
		Scanner scan = new Scanner(System.in);
		System.out.println("単語数を3、5、7の内から選んで入力してください");
		
		String str = scan.next();
		
		//3単語の場合
		if ((str.equals("3"))) {
			System.out.println("文を入力");
			str = scan.next();//入力された文字列を代入
			for(int i=0;i<4;i++){
				if(str.indexOf(word[i]) != -1){//文字を探索
					tango[0]=word[i];//配列に代入
				}
			}
			if(str.indexOf(str2) != -1){//文字を探索
				tango[1]=str2;//配列に代入
			}
			if(str.indexOf(str1) != -1){//文字を探索
				tango[2]=str1;//配列に代入
			}
			type = questioncheck(tango[1],tango[3],tango[5]);
		}
		
		//5単語の場合
		//タローの性格はなんですか
		else if ((str.equals("5"))){
			System.out.println("文を入力");
			str = scan.next();
			if(str.indexOf(taro) != -1){//文字を探索
				tango[0]=taro;//配列に代入
			}
			if(str.indexOf(str3) != -1){//文字を探索
				tango[1]=str3;//配列に代入
			}
			if(str.indexOf(seikaku,2) != -1){//文字を探索
				tango[2]=seikaku;//配列に代入
			}
			if(str.indexOf(str2,4) != -1){//文字を探索
				tango[3]=str2;//配列に代入
			}
			if(str.indexOf(str1) != -1){//文字を探索
				tango[4]=str1;//配列に代入
			}
			type = questioncheck(tango[1],tango[3],tango[5]);
		}
		//7単語の場合
		//Aさんのペットの性格はなんですか
		else if(str.equals("7")){
			System.out.println("文を入力");
			str = scan.next();
			if(str.indexOf(Asan) != -1){//文字を探索
				tango[0]=Asan;//配列に代入
			}
			if(str.indexOf(str3) != -1){//文字を探索
				tango[1]=str3;//配列に代入
			}
			if(str.indexOf(pet,4) != -1){//文字を探索
				tango[2]=pet;//配列に代入
			}
			if(str.indexOf(str3) != -1){//文字を探索
				tango[3]=str3;//配列に代入
			}
			if(str.indexOf(seikaku,6) != -1){//文字を探索
				tango[4]=seikaku;//配列に代入
			}
			if(str.indexOf(str2,7) != -1){//文字を探索
				tango[5]=str2;//配列に代入
			}
			if(str.indexOf(str1) != -1){//文字を探索
				tango[6]=str1;//配列に代入
			}
			type = questioncheck(tango[1],tango[3],tango[5]);
		}
		//タイプ別に分ける
		if (type == "A") {
			ans = new answer().hikakuObject(A,Taro,tango[0]);
			System.out.println(ans + dearu+"\n");
		}else if (type == "B") {
			ans = new answer().hikakuObject(A,Taro,tango[0],tango[2]);
			System.out.println(ans + dearu+"\n");
		}else if (type == "C") {
			ans = new answer().hikakuObject(A,Taro,tango[0],tango[2]);
			System.out.println(ans + dearu);
			ans = new answer().hikakuObject(A,Taro,ans,tango[4]);
			System.out.println(ans + dearu+"\n");
		}
	}
	
	//質問チェック
	public String questioncheck(String setuzoku1,String setuzoku2, String setuzoku3) {
		if (setuzoku1.equals(str2)) {
			return "A";
		}
		else if(setuzoku1.equals(str3)){
			if(setuzoku2.equals(str2)){
				return "B";
			}
			else if(setuzoku2.equals(str3)){
				if(setuzoku3.equals(str2)){
					return "C";
				}
			}
		}
		return "";
	}
}

//答えを導き出すクラス
class answer {
	//タイプで使うメソッド（人間オブジェクト、柴犬オブジェクト、名前、単語）
	public String hikakuObject(human human, shibainu shibainu, String namae,String tango1) { //namaeのtango1はshibainu.nameである
		//単語から答えを出す
		String object=yk(tango1);
		String index="(空)";

		switch(object){
		case "犬"://犬の場合
			index=shibainu.IndexAcquisition(tango1);//犬に関する要素を引き出す
			break;
		case "人間"://人間の場合
			index=human.IndexAcquisition(tango1);//人間に関する要素を引き出す
			break;
		}

		return index;//要素の値を返す
	}
	//タイプで使うメソッド（人間オブジェクト、柴犬オブジェクト、文字列）
	public String hikakuObject(human human,shibainu shibainu,String match) { // namaeのtango1はshibainu.nameである
		String index="(空)";

		if (match.equals(human.name)) {//文字列が人間の名前と一致した場合
			index=human.objectName;//名前を出す
		} else if (match.equals(shibainu.name)) {//文字列が犬の名前と一致した場合
			index=shibainu.objectName;//名前を出す
		}

		if (match.equals(new shibainu().kensyu)) {//文字列が柴犬の犬種と一致した場合
			index=new shibainu().objectName;//名前を出す
		}
		return index;//要素を返す
	}

	String yk(String yousomei) { //要素名に入れたものを調べる
		String objectname="(空)";

		for (int i=0;i< new dog().glossary.length;i++) {//要素分だけ取り出す
			if (yousomei.equals(new dog().glossary[i])) {//犬の中の要素とyousomeiが一致した場合
				objectname = "犬";
			}
		}

		for (int i=0;i<new human().glossary.length;i++){//要素分だけ取り出す
			if (yousomei.equals(new human().glossary[i])) {//人間の中の要素とyousomeiが一致した場合
				objectname = "人間";
			}
		}
		return objectname;//要素のオブジェクトを返す
	}
}

//人間クラス
class human {
	public String objectName="人間";//オブジェクトの名前
	public String name="無し";//名前
	public String pet="無し";//ペット
	
	String glossary[]={"ペット"};//人間の持つ要素
	
	shibainu shibainu;


	public human(){}//個別情報なしの人間オブジェクト
	
	public human(String name, String pet){//個別情報なしの人間
		this.name=name;//名前を設定
		this.pet=pet;//ペットを設定
		if(!(this.pet.equals("無し"))){ //"無し"じゃなきゃ柴犬生成
			shibainu=new shibainu(this.pet);
		}
	}
	
	String IndexAcquisition(String yousomei){//要素名と一致する項目の検索
		String syutoku="要素なし";//取得した要素の値を入れる変数
		
		switch(yousomei){
		case "ペット":
			syutoku=pet;//ペット名の取得
			break;
		default:
			break;
		}
		return syutoku;//要素を返す
	}
}

//柴犬のクラスでdogのクラスを継承する
class shibainu extends dog{

	public String kensyu="柴犬";//犬種
	public String name=null;

	public shibainu(){}//個別情報なしの柴犬オブジェクト
	
	public shibainu(String name){//個別情報ありの柴犬オブジェクト
		this.name = name;//名前の取得
	}

	String IndexAcquisition(String yousomei){//検索した要素名に該当する要素の取得
		String keisyo="要素なし";

		switch(yousomei){
		case "なん":
			keisyo=objectName;//継承されたオブジェクト名取得
			break;
		case "犬種":
			keisyo=kensyu;//犬種を取得
			break;
		case "性格":
			keisyo=seikaku;//性格を取得
			break;
		default:
			break;
		}
		return keisyo;//文字列を返す
	}
}

//犬クラス
class dog{
	public String objectName="犬";//オブジェクトの名前
	public String seikaku="従順";//性格
	public String kensyu;//犬種
	public String name;//名前
	public String glossary[] = {"性格","犬種"};//犬が持つ要素集
}