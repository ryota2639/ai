package kadai1;

//A*アルゴリズム
public class kadai1 {
	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		/*
		 * 初期状態
		 * S→0
		 * G→4
		 *
		 * コスト
		 * 山　　→3
		 * 森　　→2
		 * 空欄　→1
		 */
		//初期状態
		int start[][]={{1,1,1,1,1},
			       	   {1,1,3,1,1},
			       	   {1,1,3,2,1},
			       	   {0,2,3,2,1},
			       	   {1,1,3,2,4},
			       	   {1,1,3,2,1},
			       	   {1,1,2,1,1},
			       	   {1,1,1,1,1}};

		//現在までのコスト
		int move[][]={{17,16,15,14,13},
			           {18,17,14,13,12},
			           {19,18,15,13,11},
			           {20,18,15,13,10},
			           {19,18,15,13,9},
			           {18,17,14,12,10},
			           {17,16,14,12,11},
			           {16,15,14,13,12}};

		//未来のコスト
		int future[][]={{12,13,14,15,16},
						{13,14,15,16,17},
						{14,15,16,17,18},
						{15,16,17,18,19},
						{16,17,18,19,20},
						{15,16,17,18,19},
						{14,15,16,17,18},
						{13,14,15,16,17},};

		//合計値代入
		int sum[][] = new int[8][5];

		int i,j;

		//初期状態表示
		System.out.println("初期状態表示");
		for( i=0;i<5;i++){
			for( j=0;j<8;j++){
				switch(start[j][i]){
				case 0:System.out.print("S");break;
				case 1:System.out.print("???");break;
				case 2:System.out.print("森");break;
				case 3:System.out.print("山");break;
				case 4:System.out.print("G");break;
				}
			}
			System.out.print("\n");
		}
		System.out.print("\n");

		//合計値計算
		System.out.println("合計値");
		for(i=0;i<5;i++){
			for(j=0;j<8;j++){
				sum[j][i]=move[j][i]+future[j][i];
				System.out.print(sum[j][i]+" ");
			}
			System.out.print("\n");
		}
		System.out.print("\n");
		
		//スタートはsum[0][3]
		//最短経路の座標表示
		int max =sum[3][0];
		System.out.println("最短経路表示");
		for(i=0;i<5;i++){
			for(j=0;j<8;j++){
				if(max<=sum[j][i]){
					max=sum[j][i];
					System.out.print("("+i+","+j+")→");
				}
			}
		}
	}
}